package net.nifni.ours.rest

import com.fasterxml.jackson.databind.ObjectMapper

fun <T> T.toMap(): Map<String, Any> {
    @Suppress("UNCHECKED_CAST")
    return ObjectMapper().convertValue(this, Map::class.java) as Map<String, Any>
}