package net.nifni.ours.rest

import io.quarkus.security.runtime.QuarkusSecurityIdentity
import io.quarkus.test.junit.callback.QuarkusTestBeforeEachCallback
import io.quarkus.test.junit.callback.QuarkusTestMethodContext
import io.quarkus.test.security.TestAuthController
import io.quarkus.test.security.TestIdentityAssociation
import jakarta.enterprise.inject.spi.CDI
import org.eclipse.microprofile.jwt.Claims
import org.eclipse.microprofile.jwt.JsonWebToken
import org.jboss.logmanager.LogManager
import java.util.*


const val DEFAULT_FIRST_NAME = "firstname"
const val DEFAULT_LAST_NAME = "lastname"
const val DEFAULT_EMAIL = "test@shareours.eu"

fun useAuth(
    userId: UUID, roles: List<String> = emptyList(),
    email: String = DEFAULT_EMAIL,
    firstname: String = DEFAULT_FIRST_NAME,
    lastname: String = DEFAULT_LAST_NAME
) {
    val securityIdentity = QuarkusSecurityIdentity.builder()
        .setPrincipal(TestToken(userId, roles, email, firstname, lastname))
        .addRoles(roles.toSet())
        .build()
    CDI.current().select(TestAuthController::class.java).get().setEnabled(true)
    CDI.current().select(TestIdentityAssociation::class.java).get().testIdentity = securityIdentity
}

fun withoutAuth() {
    CDI.current().select(TestAuthController::class.java).get().setEnabled(true)
    CDI.current().select(TestIdentityAssociation::class.java).get().testIdentity = null
}

class TestToken(
    private val userId: UUID,
    private val roles: List<String>,
    private val email: String,
    private val firstname: String,
    private val lastname: String
) : JsonWebToken {
    override fun getName(): String {
        return "Test User"
    }

    override fun getClaimNames(): MutableSet<String> {
        return mutableSetOf(
            Claims.sub.name,
            Claims.groups.name,
            Claims.email.name,
            Claims.given_name.name,
            Claims.family_name.name
        )
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> getClaim(claimName: String): T? {
        return when (claimName) {
            Claims.sub.name -> userId.toString() as T
            Claims.groups.name -> roles as T
            Claims.email.name -> email as T
            Claims.given_name.name -> firstname as T
            Claims.family_name.name -> lastname as T
            else -> null
        }
    }
}

/**
 * Ensures that previous tests do not affect other tests by leaving mocked authentication in the context.
 */
class AuthCleanup : QuarkusTestBeforeEachCallback {
    override fun beforeEach(context: QuarkusTestMethodContext?) {
        LogManager.getLogManager().getLogger(this.javaClass.name).info("Cleaning mocked auth before test execution.")
        withoutAuth()
    }

}