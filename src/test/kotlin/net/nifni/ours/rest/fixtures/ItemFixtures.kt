package net.nifni.ours.rest.fixtures

import io.restassured.RestAssured
import io.restassured.http.ContentType
import net.nifni.ours.rest.toUuid
import net.nifni.ours.services.ItemCreationDto
import java.util.*

const val ITEM_NAME = "itemName"
const val ITEM_DESCRIPTION = "itemDescription"

fun createItem(userId: String): ImageItemIds {
    createUser(userId)
    val imageId = createImage(userId).toUuid()
    val createdItemId = RestAssured.given()
        .contentType(ContentType.JSON)
        .body(ItemCreationDto(ITEM_NAME, ITEM_DESCRIPTION, imageId))
        .`when`().post("/users/$userId/items")
        .then()
        .statusCode(200)
        .extract().body().jsonPath().get<String>("id")
    return ImageItemIds(imageId, createdItemId.toUuid())
}

data class ImageItemIds(val imageId: UUID, val itemId: UUID)