package net.nifni.ours.rest.fixtures

import io.restassured.RestAssured.given
import net.nifni.ours.rest.toUuid
import net.nifni.ours.rest.useAuth
import org.hamcrest.Matchers.equalTo
import java.util.*

fun createUser(externalId: UUID) {
    useAuth(externalId)
    given()
        .`when`().put("/users/$externalId")
        .then()
        .statusCode(200)
        .body("id", equalTo(externalId.toString()))
}

fun createUser(externalId: String) {
    createUser(externalId.toUuid())
}