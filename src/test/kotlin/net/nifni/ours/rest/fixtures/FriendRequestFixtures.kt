package net.nifni.ours.rest.fixtures

import io.restassured.RestAssured
import io.restassured.http.ContentType
import net.nifni.ours.rest.resources.FriendRequestUpdateDto
import net.nifni.ours.rest.resources.FriendUpdateType
import net.nifni.ours.rest.toUuid
import net.nifni.ours.rest.useAuth
import net.nifni.ours.services.FriendRequestCreationDto
import java.util.*


fun createFriendRequest(creator: UUID, friend: UUID): String {
    useAuth(creator)
    return RestAssured.given()
        .contentType(ContentType.JSON)
        .body(FriendRequestCreationDto(friend))
        .`when`().post("/users/$creator/friendrequests")
        .then()
        .statusCode(200)
        .extract().body().jsonPath().get("id")
}

fun acceptFriendRequest(creator: UUID, friend: UUID, friendRequestId: String) {
    useAuth(friend)
    RestAssured.given()
        .contentType(ContentType.JSON)
        .body(FriendRequestUpdateDto(FriendUpdateType.ACCEPT))
        .`when`().put("users/$creator/friendrequests/$friendRequestId")
        .then()
        .statusCode(200)
}