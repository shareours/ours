package net.nifni.ours.rest.fixtures

import net.nifni.ours.rest.toUuid
import net.nifni.ours.rest.useAuth
import java.util.UUID

fun createFriendship(firstUser: UUID, secondUser: UUID) {
    useAuth(firstUser)
    val friendRequestId = createFriendRequest(firstUser, secondUser)
    acceptFriendRequest(firstUser, secondUser, friendRequestId)
}
