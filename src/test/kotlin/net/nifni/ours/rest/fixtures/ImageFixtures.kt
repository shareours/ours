package net.nifni.ours.rest.fixtures

import io.restassured.RestAssured
import org.hamcrest.Matchers
import java.io.File


private val pngFile = File(object {}::class.java.classLoader.getResource("test.png")!!.toURI());


fun createImage(userId: String): String {
    return RestAssured.given()
        .multiPart("image", pngFile)
        .`when`().post("/images/user/$userId")
        .then()
        .statusCode(200)
        .body("id", Matchers.notNullValue())
        .extract().body().jsonPath().get("id")
}