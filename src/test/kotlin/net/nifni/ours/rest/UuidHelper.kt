package net.nifni.ours.rest

import java.util.*

fun String.toUuid(): UUID {
    return UUID.fromString(this)
}