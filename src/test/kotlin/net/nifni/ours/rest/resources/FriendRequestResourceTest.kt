package net.nifni.ours.rest.resources

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured
import io.restassured.http.ContentType
import net.nifni.ours.rest.*
import net.nifni.ours.rest.fixtures.createFriendRequest
import net.nifni.ours.rest.fixtures.createFriendship
import net.nifni.ours.rest.fixtures.createUser
import net.nifni.ours.services.FriendRequestCreationDto
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.util.*


@QuarkusTest
class FriendRequestResourceTest {

    private lateinit var firstUser: UUID
    private lateinit var secondUser: UUID

    @BeforeEach
    fun createUsers() {
        firstUser = UUID.randomUUID()
        secondUser = UUID.randomUUID()
        createUser(firstUser.toString())
        createUser(secondUser.toString())
        withoutAuth()
    }

    @Nested
    inner class CreateFriendRequest {

        @Test
        fun `create friendrequest with yourself - 400`() {
            useAuth(firstUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestCreationDto(firstUser))
                .`when`().post("/users/$firstUser/friendrequests")
                .then()
                .statusCode(400)
        }

        @Test
        fun `create friendrequest with not existing user - 404`() {
            val notExistingUser = UUID.randomUUID()
            useAuth(notExistingUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestCreationDto(firstUser))
                .`when`().post("/users/$notExistingUser/friendrequests")
                .then()
                .statusCode(404)

            useAuth(firstUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestCreationDto(notExistingUser))
                .`when`().post("/users/$firstUser/friendrequests")
                .then()
                .statusCode(404)
        }

        @Test
        fun `create friendrequest without auth - 401`() {
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestCreationDto(secondUser))
                .`when`().post("/users/$firstUser/friendrequests")
                .then()
                .statusCode(401)
        }

        @Test
        fun `with not matching user id in token - 403`() {
            useAuth(secondUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestCreationDto(secondUser))
                .`when`().post("/users/$firstUser/friendrequests")
                .then()
                .statusCode(403)
        }

        @Test
        fun `with matching user id in token - success`() {
            useAuth(firstUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestCreationDto(secondUser))
                .`when`().post("/users/$firstUser/friendrequests")
                .then()
                .statusCode(200)
        }

        @Test
        fun `with friend request between users already existing - conflict`() {
            createFriendRequest(firstUser, secondUser)
            useAuth(firstUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestCreationDto(secondUser))
                .`when`().post("/users/$firstUser/friendrequests")
                .then()
                .statusCode(409)

            useAuth(secondUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestCreationDto(firstUser))
                .`when`().post("/users/$secondUser/friendrequests")
                .then()
                .statusCode(409)
        }

        @Test
        fun `with already befreiended users - conflict`() {
            createFriendship(firstUser, secondUser)
            useAuth(firstUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestCreationDto(secondUser))
                .`when`().post("/users/$firstUser/friendrequests")
                .then()
                .statusCode(409)

            useAuth(secondUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestCreationDto(firstUser))
                .`when`().post("/users/$secondUser/friendrequests")
                .then()
                .statusCode(409)
        }
    }

    @Nested
    inner class AcceptFriendRequest {

        @Test
        fun `with not existing friendrequest - not found`() {
            useAuth(secondUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.ACCEPT))
                .`when`().put("users/$firstUser/friendrequests/${UUID.randomUUID()}")
                .then()
                .statusCode(404)
        }

        @Test
        fun `with friendrequest existing but for different user - not found`() {
            val friendRequestId = createFriendRequest(firstUser, secondUser)
            useAuth(secondUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.ACCEPT))
                .`when`().put("users/$secondUser/friendrequests/$friendRequestId")
                .then()
                .statusCode(404)
        }

        @Test
        fun `with matching user id in token - success`() {
            val friendRequestId = createFriendRequest(firstUser, secondUser)
            useAuth(secondUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.ACCEPT))
                .`when`().put("users/$firstUser/friendrequests/$friendRequestId")
                .then()
                .statusCode(200)
        }

        @Test
        fun `with creator wanting to accept - forbidden`() {
            val friendRequestId = createFriendRequest(firstUser, secondUser)
            useAuth(firstUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.ACCEPT))
                .`when`().put("users/$firstUser/friendrequests/$friendRequestId")
                .then()
                .statusCode(403)
                .body("message", Matchers.equalTo("Not authorized to do the given action."))
                .body(
                    "details",
                    Matchers.equalTo(
                        hashMapOf(
                            "tokenSubject" to firstUser.toString(),
                            "requiredUserId" to secondUser.toString()
                        )
                    )
                )
        }

        @Test
        fun `without authentication - unauthorized`() {
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.ACCEPT))
                .`when`().put("users/$firstUser/friendrequests/${UUID.randomUUID()}")
                .then()
                .statusCode(401)
        }

        @Test
        fun `with matching user id in token but not existing friend request - not found`() {
            useAuth(secondUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.ACCEPT))
                .`when`().put("users/$firstUser/friendrequests/${UUID.randomUUID()}")
                .then()
                .statusCode(404)
        }
    }

    @Nested
    inner class DeclineFriendRequest {

        @Test
        fun `with not existing friendrequest - not found`() {
            useAuth(secondUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.DECLINE))
                .`when`().put("users/$firstUser/friendrequests/${UUID.randomUUID()}")
                .then()
                .statusCode(404)
        }

        @Test
        fun `with matching user id in token - success`() {
            val friendRequestId = createFriendRequest(firstUser, secondUser)
            useAuth(secondUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.DECLINE))
                .`when`().put("users/$firstUser/friendrequests/$friendRequestId")
                .then()
                .statusCode(200)
        }

        @Test
        fun `with creator wanting to accept - forbidden`() {
            val friendRequestId = createFriendRequest(firstUser, secondUser)
            useAuth(firstUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.DECLINE))
                .`when`().put("users/$firstUser/friendrequests/$friendRequestId")
                .then()
                .statusCode(403)
                .body("message", Matchers.equalTo("Not authorized to do the given action."))
                .body(
                    "details",
                    Matchers.equalTo(
                        hashMapOf(
                            "tokenSubject" to firstUser.toString(),
                            "requiredUserId" to secondUser.toString()
                        )
                    )
                )
        }

        @Test
        fun `without authentication - unauthorized`() {
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.DECLINE))
                .`when`().put("users/$firstUser/friendrequests/${UUID.randomUUID()}")
                .then()
                .statusCode(401)
        }

        @Test
        fun `with matching user id in token but not existing friend request - not found`() {
            useAuth(secondUser)
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(FriendRequestUpdateDto(FriendUpdateType.DECLINE))
                .`when`().put("users/$firstUser/friendrequests/${UUID.randomUUID()}")
                .then()
                .statusCode(404)
        }
    }

    @Nested
    inner class GetFriendRequests {

        @Test
        fun `with user who received the friend request - success`() {
            val friendRequestId = createFriendRequest(firstUser, secondUser)
            useAuth(secondUser)
            RestAssured.given()
                .`when`().get("users/$secondUser/friendrequests")
                .then()
                .statusCode(200)
                .body(
                    "friendRequests",
                    Matchers.hasItem(
                        FriendRequestResponseDto(
                            friendRequestId.toUuid(),
                            firstUser,
                            FriendRequestType.RECEIVE,
                            DEFAULT_FIRST_NAME,
                            DEFAULT_LAST_NAME
                        ).toMap()
                    )
                )
        }

        @Test
        fun `with user who sent the friend request - success with empty list`() {
            createFriendRequest(firstUser, secondUser)
            useAuth(firstUser)
            RestAssured.given()
                .`when`().get("users/$firstUser/friendrequests")
                .then()
                .statusCode(200)
                .body("friendRequests", Matchers.empty<Any>())
        }

        @Test
        fun `with authentication for other user - forbidden`() {
            useAuth(firstUser)
            RestAssured.given()
                .`when`().get("users/$secondUser/friendrequests")
                .then()
                .statusCode(403)
        }

        @Test
        fun `without authentication - unauthorized`() {
            RestAssured.given()
                .`when`().get("users/$firstUser/friendrequests")
                .then()
                .statusCode(401)
        }
    }

    @Nested
    inner class DeleteFriendRequest {

        private lateinit var friendRequestId: String

        @BeforeEach
        fun createFriendRequest() {
            friendRequestId = createFriendRequest(firstUser, secondUser)
        }

        @Test
        fun `with user who created the friend request - success`() {
            useAuth(firstUser)
            RestAssured.given()
                .`when`().delete("users/$firstUser/friendrequests/$friendRequestId")
                .then()
                .statusCode(200)

            RestAssured.given()
                .`when`().get("users/$firstUser/friendrequests")
                .then()
                .statusCode(200)
                .body(
                    "friendRequests",
                    Matchers.empty<Any>()
                )
        }

        @Test
        fun `with not existing friend request id - not found`() {
            val notExistingFriendRequestId = UUID.randomUUID()
            useAuth(firstUser)
            RestAssured.given()
                .`when`().delete("users/$firstUser/friendrequests/$notExistingFriendRequestId")
                .then()
                .statusCode(404)
        }

        @Test
        fun `with authentication for other user - forbidden`() {
            useAuth(firstUser)
            RestAssured.given()
                .`when`().delete("users/$secondUser/friendrequests/$friendRequestId")
                .then()
                .statusCode(403)
        }

        @Test
        fun `without authentication - unauthorized`() {
            withoutAuth()
            RestAssured.given()
                .`when`().delete("users/$firstUser/friendrequests/$friendRequestId")
                .then()
                .statusCode(401)
        }
    }
}