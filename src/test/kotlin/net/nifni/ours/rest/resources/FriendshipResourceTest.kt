package net.nifni.ours.rest.resources

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured
import net.nifni.ours.rest.*
import net.nifni.ours.rest.fixtures.createFriendship
import net.nifni.ours.rest.fixtures.createUser
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.util.*

@QuarkusTest
class FriendshipResourceTest {

    private lateinit var userId: UUID
    private lateinit var friendId: UUID

    @BeforeEach
    fun setupUsers() {
        userId = UUID.randomUUID()
        friendId = UUID.randomUUID()
        createUser(friendId.toString())
        createUser(userId.toString())
    }

    @Nested
    inner class GetFriendships {
        @Test
        fun `when friendship exists and user is authenticated - success`() {
            createFriendship(userId, friendId)
            useAuth(userId)
            RestAssured.given()
                .`when`().get("/users/$userId/friendships")
                .then()
                .statusCode(200)
                .body(
                    "friendships",
                    Matchers.hasSize<Any>(1),
                    "friendships",
                    Matchers.hasItem(FriendshipResponseDto(friendId, DEFAULT_FIRST_NAME, DEFAULT_LAST_NAME).toMap())
                )
        }

        @Test
        fun `when no friendship exists - success with empty list`() {
            useAuth(userId)
            RestAssured.given()
                .`when`().get("/users/$userId/friendships")
                .then()
                .statusCode(200)
                .body("friendships", Matchers.hasSize<Any>(0))
        }

        @Test
        fun `without auth - unauthorized`() {
            withoutAuth()
            RestAssured.given()
                .`when`().get("/users/$userId/friendships")
                .then()
                .statusCode(401)
        }

        @Test
        fun `when getting friendships for other user - forbidden`() {
            useAuth(friendId)
            RestAssured.given()
                .`when`().get("/users/$userId/friendships")
                .then()
                .statusCode(403)
        }
    }

    @Nested
    inner class DeleteFriendship {


        private lateinit var notExistingUserId: UUID
        private lateinit var notExistingFriendId: UUID

        @BeforeEach
        fun setupNotExistingUsers() {
            notExistingUserId = UUID.randomUUID()
            notExistingFriendId = UUID.randomUUID()
        }

        @Test
        fun `when friendship exists and user is authenticated - success`() {
            createFriendship(userId, friendId)
            useAuth(userId)
            RestAssured.given()
                .`when`().delete("/users/$userId/friendships/$friendId")
                .then()
                .statusCode(200)

            RestAssured.given()
                .`when`().get("/users/$userId/friendships")
                .then()
                .statusCode(200)
                .body(
                    "friendships", Matchers.empty<Any>()
                )
        }

        @Test
        fun `when friendship does not exist - success`() {
            useAuth(userId)
            RestAssured.given()
                .`when`().delete("/users/$userId/friendships/$friendId")
                .then()
                .statusCode(200)
        }

        @Test
        fun `when user does not exist - not found`() {
            useAuth(notExistingUserId)
            RestAssured.given()
                .`when`().delete("/users/$notExistingUserId/friendships/$friendId")
                .then()
                .statusCode(404)
        }

        @Test
        fun `when friend does not exist - not found`() {
            useAuth(userId)
            RestAssured.given()
                .`when`().delete("/users/$userId/friendships/$notExistingFriendId")
                .then()
                .statusCode(404)
        }

        @Test
        fun `without auth - unauthorized`() {
            withoutAuth()
            RestAssured.given()
                .`when`().delete("/users/$userId/friendships/$friendId")
                .then()
                .statusCode(401)
        }

        @Test
        fun `when deleting friendships for other user - forbidden`() {
            useAuth(friendId)
            RestAssured.given()
                .`when`().delete("/users/$userId/friendships/$friendId")
                .then()
                .statusCode(403)
        }
    }
}