package net.nifni.ours.rest.resources

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured
import io.restassured.http.ContentType
import net.nifni.ours.rest.fixtures.*
import net.nifni.ours.rest.toUuid
import net.nifni.ours.rest.useAuth
import net.nifni.ours.services.ItemCreationDto
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.util.*


@QuarkusTest
class ItemResourceTest {

    private lateinit var firstUserId: String
    private lateinit var secondUserId: String

    @BeforeEach
    fun generateUserIds() {
        firstUserId = UUID.randomUUID().toString()
        secondUserId = UUID.randomUUID().toString()
    }

    @Nested
    inner class UploadAndRetrieveItem {

        @Test
        fun `without auth - 401`() {
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(ItemCreationDto(ITEM_NAME, ITEM_DESCRIPTION, UUID.randomUUID()))
                .`when`().post("/users/$firstUserId/items")
                .then()
                .statusCode(401)
        }

        @Test
        fun `with not matching user id in token - 403`() {
            useAuth(secondUserId.toUuid())
            RestAssured.given()
                .contentType(ContentType.JSON)
                .body(ItemCreationDto(ITEM_NAME, ITEM_DESCRIPTION, UUID.randomUUID()))
                .`when`().post("/users/$firstUserId/items")
                .then()
                .statusCode(403)
                .body("status", equalTo(403))
                .body("code", equalTo("UNAUTHORIZED"))
                .body("message", equalTo("Subject does not match id in path."))
                .body("details", equalTo(hashMapOf("subject" to secondUserId, "id" to firstUserId)))

        }

        @Test
        fun `with matching user id in token - success`() {
            createUser(firstUserId)
            useAuth(firstUserId.toUuid())
            val imageItemIds = createItem(firstUserId)

            RestAssured.given()
                .`when`().get("/users/$firstUserId/items")
                .then()
                .statusCode(200)
                .body(
                    "items", hasSize<Any>(1),
                    "items[0]", aMapWithSize<Any, Any>(5),
                    "items[0].name", equalTo(ITEM_NAME),
                    "items[0].description", equalTo(ITEM_DESCRIPTION),
                    "items[0].image", equalTo(imageItemIds.imageId.toString()),
                    "items[0].owner", equalTo(firstUserId),
                    "items[0].id", equalTo(imageItemIds.itemId.toString()),
                )
        }

        @Test
        fun `with not existing friend - not found`() {
            useAuth(firstUserId.toUuid())

            RestAssured.given()
                .`when`().get("/users/$secondUserId/items")
                .then()
                .statusCode(403)
        }

        @Test
        fun `with not existing friendship - forbidden`() {
            createUser(firstUserId)
            createUser(secondUserId)
            useAuth(firstUserId.toUuid())

            RestAssured.given()
                .`when`().get("/users/$secondUserId/items")
                .then()
                .statusCode(403)
        }

        @Test
        fun `with existing friendship - success`() {
            createUser(firstUserId)
            createUser(secondUserId)
            createFriendship(firstUserId.toUuid(), secondUserId.toUuid())
            val imageItemIds = createItem(secondUserId)

            useAuth(firstUserId.toUuid())
            RestAssured.given()
                .`when`().get("/users/$secondUserId/items")
                .then()
                .statusCode(200)
                .body(
                    "items", hasSize<Any>(1),
                    "items[0]", aMapWithSize<Any, Any>(5),
                    "items[0].name", equalTo(ITEM_NAME),
                    "items[0].description", equalTo(ITEM_DESCRIPTION),
                    "items[0].image", equalTo(imageItemIds.imageId.toString()),
                    "items[0].owner", equalTo(secondUserId),
                    "items[0].id", equalTo(imageItemIds.itemId.toString()),
                )
        }
    }

    @Nested
    inner class DeleteItem {
        @Test
        fun `with existing item - success`() {
            useAuth(firstUserId.toUuid())
            val imageItemIds = createItem(firstUserId)
            RestAssured.given()
                .`when`().delete("/users/${firstUserId}/items/${imageItemIds.itemId}")
                .then()
                .statusCode(200)
                .body(emptyOrNullString())

            RestAssured.given()
                .`when`().get("/users/$firstUserId/items")
                .then()
                .statusCode(200)
                .body("items", hasSize<Any>(0))

            RestAssured.given()
                .`when`().get("/images/${imageItemIds.imageId}")
                .then()
                .statusCode(404)
        }

        @Test
        fun `with invalid user - 403`() {
            useAuth(firstUserId.toUuid())
            RestAssured.given()
                .`when`().delete("/users/${secondUserId}/items/${UUID.randomUUID()}")
                .then()
                .statusCode(403)
        }

        @Test
        fun `with existing item of different user - 404`() {
            val imageItemIds = createItem(firstUserId)
            useAuth(secondUserId.toUuid())
            RestAssured.given()
                .`when`().delete("/users/${secondUserId}/items/${imageItemIds.itemId}")
                .then()
                .statusCode(404)
        }

        @Test
        fun `without authentication - 401`() {
            RestAssured.given()
                .`when`().delete("/users/${firstUserId}/items/${UUID.randomUUID()}")
                .then()
                .statusCode(401)
        }

        @Test
        fun `with not existing item - 404`() {
            useAuth(firstUserId.toUuid())
            RestAssured.given()
                .`when`().delete("/users/${firstUserId}/items/${UUID.randomUUID()}")
                .then()
                .statusCode(404)
        }


    }
}