package net.nifni.ours.rest.resources

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import net.nifni.ours.rest.fixtures.createUser
import net.nifni.ours.rest.useAuth
import net.nifni.ours.rest.withoutAuth
import org.hamcrest.Matchers.equalTo
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.util.*

private const val EMAIL = "test@testmail.test"
private const val FIRST_NAME = "someOtherFirstName"
private const val LAST_NAME = "someOtherLastName"

@QuarkusTest
class UserResourceTest {


    private val userId1 = UUID.randomUUID()
    private val userId2 = UUID.randomUUID()

    @Nested
    inner class Put {

        @Test
        fun `without auth - 401`() {
            withoutAuth()
            given()
                .`when`().put("/users/$userId1")
                .then()
                .statusCode(401)
        }

        @Test
        fun `with not matching user id in token - 403`() {
            useAuth(userId2)
            given()
                .`when`().put("/users/$userId1")
                .then()
                .statusCode(403)
                .body("status", equalTo(403))
                .body("code", equalTo("UNAUTHORIZED"))
                .body("message", equalTo("Subject does not match id in path."))
                .body("details", equalTo(hashMapOf("subject" to userId2.toString(), "id" to userId1.toString())))

        }

        @Test
        fun `with matching user id in token - success`() {
            useAuth(userId1)
            given()
                .`when`().put("/users/$userId1")
                .then()
                .statusCode(200)
                .body("id", equalTo(userId1.toString()))

        }

        @Test
        fun `with existing user updates fields - success`() {
            createUser(userId1)
            useAuth(
                userId1,
                email = EMAIL,
                firstname = FIRST_NAME,
                lastname = LAST_NAME
            )
            given()
                .`when`().put("/users/$userId1")
                .then()
                .statusCode(200)
                .body("id", equalTo(userId1.toString()))
                .body("email", equalTo(EMAIL))
                .body("firstname", equalTo(FIRST_NAME))
                .body("lastname", equalTo(LAST_NAME))
        }
    }
}