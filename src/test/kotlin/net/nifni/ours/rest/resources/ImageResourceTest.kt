package net.nifni.ours.rest.resources

import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured
import net.nifni.ours.rest.fixtures.createUser
import net.nifni.ours.rest.toUuid
import net.nifni.ours.rest.useAuth
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Files
import java.util.*


@QuarkusTest
class ImageResourceTest {

    private lateinit var pngFile: File

    private lateinit var USER_ID_1: String
    private lateinit var USER_ID_2: String

    @BeforeEach
    fun generateUserIds() {
        USER_ID_1 = UUID.randomUUID().toString()
        USER_ID_2 = UUID.randomUUID().toString()
    }

    @BeforeEach
    fun loadTestPng() {
        val resource = this::class.java.classLoader.getResource("test.png")
        pngFile = File(resource!!.toURI())
    }

    @Nested
    inner class UploadAndRetrieveFile {

        @Test
        fun `without auth - 401`() {
            RestAssured.given()
                .multiPart("image", pngFile)
                .`when`().post("/images/user/${USER_ID_1}")
                .then()
                .statusCode(401)
        }

        @Test
        fun `with not matching user id in token - 403`() {
            useAuth(USER_ID_2.toUuid())
            RestAssured.given()
                .multiPart("image", pngFile)
                .`when`().post("/images/user/$USER_ID_1")
                .then()
                .statusCode(403)
                .body("status", Matchers.equalTo(403))
                .body("code", Matchers.equalTo("UNAUTHORIZED"))
                .body("message", Matchers.equalTo("Subject does not match id in path."))
                .body("details", Matchers.equalTo(hashMapOf("subject" to USER_ID_2, "id" to USER_ID_1)))

        }

        @Test
        fun `with matching user id in token - success`() {
            createUser(USER_ID_1)
            useAuth(USER_ID_1.toUuid())
            val createdImageId = RestAssured.given()
                .multiPart("image", pngFile)
                .`when`().post("/images/user/$USER_ID_1")
                .then()
                .statusCode(200)
                .body("id", Matchers.notNullValue())
                .extract().body().jsonPath().get<String>("id")

            val image = RestAssured.given()
                .`when`().get("/images/$createdImageId")
                .then()
                .statusCode(200)
                .extract().body().asByteArray()

            assertThat(image, Matchers.equalTo(Files.readAllBytes(pngFile.toPath())))
        }
    }
}