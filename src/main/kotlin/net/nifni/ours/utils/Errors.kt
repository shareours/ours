package net.nifni.ours.utils

import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.Status
import net.nifni.ours.rest.ErrorCode
import net.nifni.ours.rest.ErrorDto
import java.util.*

sealed interface Errors {

    fun toResponse(): Response

    data class NotFoundError(val unit: String, val id: String) : Errors {
        override fun toResponse(): Response {
            return ErrorDto.from(Status.NOT_FOUND, ErrorCode.NOT_FOUND, "$unit with id $id not found")
        }
    }

    data class NotAuthorizedError(val requiredUserId: UUID, val tokenSubject: String) : Errors {
        override fun toResponse(): Response {
            return ErrorDto.from(
                Status.FORBIDDEN,
                ErrorCode.UNAUTHORIZED,
                "Not authorized to do the given action.",
                hashMapOf("tokenSubject" to tokenSubject, "requiredUserId" to requiredUserId.toString())
            )
        }
    }

    data class FriendRequestConflictError(val firstUser: UUID, val secondUser: UUID) : Errors {
        override fun toResponse(): Response {
            return ErrorDto.from(
                Status.CONFLICT,
                ErrorCode.CONFLICT,
                "The given object could not be created because it already exists.",
                hashMapOf(
                    "firstUser" to firstUser.toString(),
                    "secondUser" to secondUser.toString()
                )
            )
        }
    }

    data object AuthenticationMissingError : Errors {
        override fun toResponse(): Response {
            return ErrorDto.from(
                Status.UNAUTHORIZED,
                ErrorCode.AUTHENTICATION_MISSING,
                "JWT token is missing or could not be processed"
            )
        }
    }

    data class ClaimMissingError(val claim: String) : Errors {
        override fun toResponse(): Response {
            return ErrorDto.from(
                Status.BAD_REQUEST,
                ErrorCode.CLAIM_MISSING,
                "Claim is missing from token",
                mapOf("claim" to claim)
            )
        }
    }

    data class BadRequestError(val errorCode: ErrorCode, val message: String) : Errors {
        override fun toResponse(): Response {
            return ErrorDto.from(Status.BAD_REQUEST, ErrorCode.SAME_USER_FRIENDREQUEST, message)
        }
    }
}
