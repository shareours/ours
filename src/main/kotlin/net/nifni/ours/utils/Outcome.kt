package net.nifni.ours.utils

import jakarta.ws.rs.core.Response

class Outcome<T>(private val error: Errors?, private val value: T?) {

    fun isError(): Boolean = error != null
    fun isSuccess(): Boolean = !isError()

    fun getError(): Errors {
        return error!!
    }

    fun <R> map(mapping: (T) -> R): Outcome<R> {
        if (isError()) {
            return error(getError())
        }
        return success(mapping.invoke(value!!))
    }

    fun <RESULT> flatMap(mapping: (T) -> Outcome<RESULT>): Outcome<RESULT> {
        if (isError()) {
            return error(getError())
        }
        return mapping.invoke(value!!)
    }

    fun mapError(mapping: (Errors) -> Errors): Outcome<T> {
        if (isError()) {
            return error(mapping.invoke(error!!))
        }
        return this
    }

    fun onSuccess(mapping: (T) -> Unit): Outcome<T> {
        if (isSuccess()) {
            mapping.invoke(value!!)
        }
        return this
    }

    fun onError(mapping: () -> Outcome<T>): Outcome<T> {
        if (isError()) {
            return mapping.invoke()
        }
        return this
    }

    fun getErrorResponse(): Response? {
        if (isError()) {
            return error!!.toResponse()
        }
        return null
    }

    fun toResponse(valueMapping: (T) -> Response): Response {
        return getErrorResponse() ?: valueMapping.invoke(value!!)
    }

    companion object {
        fun <T> error(error: Errors): Outcome<T> {
            return Outcome(error, null)
        }

        fun success(): Outcome<Unit> {
            return Outcome(null, Unit)
        }

        fun <T> success(value: T): Outcome<T> {
            return Outcome(null, value)
        }

        fun <T> notFound(unit: String, id: String): Outcome<T> {
            return error(Errors.NotFoundError(unit, id))
        }
    }
}
