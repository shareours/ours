package net.nifni.ours.database.friendship

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepositoryBase
import jakarta.enterprise.context.ApplicationScoped

@ApplicationScoped
class FriendshipRepository : PanacheRepositoryBase<FriendshipModel, Long> {
    fun findByUser(user: Long): List<FriendshipModel> {
        return find("userId", user).list()
    }

    fun findByUserAndFriend(user: Long, friend: Long): FriendshipModel? {
        return find("userId = ?1 and friendId = ?2 ", user, friend).firstResult()
    }

    fun existsByUserAndFriend(user: Long, friend: Long): Boolean {
        return count("userId = ?1 and friendId = ?2", user, friend) > 0
    }
}