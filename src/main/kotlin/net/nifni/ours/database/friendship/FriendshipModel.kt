package net.nifni.ours.database.friendship

import jakarta.persistence.Column
import jakarta.persistence.Entity
import net.nifni.ours.database.BaseEntity
import java.util.*

@Entity(name = "friendships")
class FriendshipModel(
    @Column(name = "external_id", nullable = false, columnDefinition = "uuid")
    var externalId: UUID,
    @Column(name = "user_id", nullable = false, columnDefinition = "bigint")
    var userId: Long,
    @Column(name = "friend_id", nullable = false, columnDefinition = "bigint")
    var friendId: Long
) : BaseEntity()