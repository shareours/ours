package net.nifni.ours.database.user

import jakarta.persistence.Column
import jakarta.persistence.Entity
import net.nifni.ours.database.BaseEntity
import java.util.UUID

@Entity(name = "users")
class UserModel(
    @Column(nullable = false, name = "external_id", columnDefinition = "uuid")
    var externalId: UUID,
    @Column(nullable = false, name = "email", columnDefinition = "text")
    var email: String,
    @Column(nullable = false, name = "firstname", columnDefinition = "text")
    var firstname: String,
    @Column(nullable = false, name = "lastname", columnDefinition = "text")
    var lastname: String
) : BaseEntity()
