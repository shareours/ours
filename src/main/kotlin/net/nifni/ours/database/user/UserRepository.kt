package net.nifni.ours.database.user

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import jakarta.enterprise.context.ApplicationScoped
import java.util.UUID

@ApplicationScoped
class UserRepository : PanacheRepository<UserModel> {

    fun findByExternalId(externalId: UUID): UserModel? {
        return find("externalId", externalId).firstResult()
    }
}