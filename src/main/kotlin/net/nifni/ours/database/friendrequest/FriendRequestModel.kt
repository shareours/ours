package net.nifni.ours.database.friendrequest

import jakarta.persistence.Column
import jakarta.persistence.Entity
import net.nifni.ours.database.BaseEntity
import java.util.*

@Entity(name = "friendrequests")
class FriendRequestModel(
    @Column(name = "external_id", nullable = false, columnDefinition = "uuid")
    var externalId: UUID,
    @Column(nullable = false, columnDefinition = "bigint")
    var creator: Long,
    @Column(nullable = false, columnDefinition = "bigint")
    var friend: Long
) : BaseEntity()
