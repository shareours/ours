package net.nifni.ours.database.friendrequest

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepositoryBase
import jakarta.enterprise.context.ApplicationScoped
import java.util.*

@ApplicationScoped
class FriendRequestRepository : PanacheRepositoryBase<FriendRequestModel, Long> {
    fun findByExternalId(externalId: UUID): FriendRequestModel? {
        return find("externalId", externalId).firstResult()
    }

    fun findByFriend(friend: Long): List<FriendRequestModel> {
        return find("friend", friend).list()
    }

    fun deleteByExternalId(externalId: UUID) {
        delete("externalId", externalId)
    }

    fun findByUserIds(firstUser: Long, secondUser: Long): FriendRequestModel? {
        return find(
            "(creator = ?1 and friend = ?2) or (creator = ?2 and friend = ?1)",
            firstUser,
            secondUser
        ).firstResult()
    }
}
