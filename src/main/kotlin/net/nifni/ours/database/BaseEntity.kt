package net.nifni.ours.database

import io.quarkus.hibernate.orm.panache.kotlin.PanacheEntityBase
import jakarta.persistence.Column
import jakarta.persistence.Id
import jakarta.persistence.MappedSuperclass
import org.hibernate.annotations.Generated
import java.time.Instant

@MappedSuperclass
abstract class BaseEntity(
    @Id
    @Generated
    @Column(nullable = false, unique = true, columnDefinition = "bigint")
    var id: Long? = null,
    @Column(nullable = false, name = "created_at")
    var createdAt: Instant = Instant.now()
) : PanacheEntityBase