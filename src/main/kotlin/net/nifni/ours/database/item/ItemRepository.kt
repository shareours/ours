package net.nifni.ours.database.item

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import jakarta.enterprise.context.ApplicationScoped
import java.util.UUID

@ApplicationScoped
class ItemRepository : PanacheRepository<ItemModel> {

    fun findByOwner(owner: Long): List<ItemModel> {
        return find("owner", owner).list()
    }

    fun findByExternalIdAndUser(externalId: UUID, owner: Long): ItemModel? {
        return find("externalId = ?1 and owner = ?2", externalId, owner).firstResult()
    }
}