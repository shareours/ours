package net.nifni.ours.database.item

import jakarta.persistence.Column
import jakarta.persistence.Entity
import net.nifni.ours.database.BaseEntity
import java.util.*

@Entity(name = "items")
class ItemModel(
    @Column(nullable = false, name = "external_id", columnDefinition = "uuid")
    var externalId: UUID,
    @Column(nullable = false, columnDefinition = "text")
    var name: String,
    @Column(nullable = false, columnDefinition = "text")
    var description: String,
    @Column(nullable = false, columnDefinition = "bigint")
    var owner: Long,
    @Column(nullable = false, columnDefinition = "bigint")
    var image: Long
) : BaseEntity()
