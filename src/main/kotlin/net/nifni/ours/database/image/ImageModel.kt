package net.nifni.ours.database.image

import jakarta.persistence.Column
import jakarta.persistence.Entity
import net.nifni.ours.database.BaseEntity
import java.util.*

@Entity(name = "images")
class ImageModel(
    @Column(name = "external_id", nullable = false, columnDefinition = "uuid")
    var externalId: UUID,
    @Column(nullable = false, columnDefinition = "bytea")
    var data: ByteArray,
    @Column(nullable = false, columnDefinition = "bigint")
    var creator: Long
) : BaseEntity()