package net.nifni.ours.database.image

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepositoryBase
import jakarta.enterprise.context.ApplicationScoped
import java.util.*

@ApplicationScoped
class ImageRepository : PanacheRepositoryBase<ImageModel, Long> {
    fun findByExternalId(externalId: UUID): ImageModel? {
        return find("externalId", externalId).firstResult()
    }
}