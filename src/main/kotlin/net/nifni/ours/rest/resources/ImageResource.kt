package net.nifni.ours.rest.resources

import io.quarkus.security.Authenticated
import jakarta.transaction.Transactional
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import net.nifni.ours.rest.Authorizer
import net.nifni.ours.rest.ErrorDto
import net.nifni.ours.services.ImageService
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import org.jboss.resteasy.reactive.RestForm
import org.jboss.resteasy.reactive.multipart.FileUpload
import java.nio.file.Files
import java.util.*

@Path("/images")
class ImageResource(
    private val authorizer: Authorizer,
    private val imageService: ImageService
) {

    @Authenticated
    @POST
    @Path("/user/{userId}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Transactional
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Image was uploaded successfully",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ImageIdDto::class))]
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "403",
            description = "User is not allowed to upload images",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        ),
        APIResponse(
            responseCode = "404",
            description = "User not found"
        )
    )
    @Operation(summary = "Upload Image")
    fun createImage(userId: UUID, @RestForm image: FileUpload): Response {
        authorizer.checkUserAuthorization(userId)?.let { return it }
        val byteArray = Files.readAllBytes(image.filePath())
        imageService.createImage(byteArray, userId)?.let {
            return Response.ok().entity(ImageIdDto(it.externalId)).build()
        }
        return Response.status(Response.Status.NOT_FOUND).build()
    }

    @GET
    @Path("/{imageId}")
    @Produces("image/png")
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Image was found",
            content = [Content(mediaType = "image/png")]
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "404",
            description = "The image ID does not exist",
            content = [Content(mediaType = "application/json")]
        )
    )
    @Operation(summary = "Get Image")
    fun getImage(imageId: UUID): Response {
        val imageModel = imageService.getImage(imageId) ?: return Response.status(Response.Status.NOT_FOUND).build()
        return Response.ok().entity(imageModel.data).build()
    }

}

data class ImageIdDto(
    val id: UUID
)