package net.nifni.ours.rest.resources

import io.quarkus.security.Authenticated
import jakarta.transaction.Transactional
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import net.nifni.ours.database.friendrequest.FriendRequestModel
import net.nifni.ours.rest.Authorizer
import net.nifni.ours.rest.ErrorDto
import net.nifni.ours.services.FriendRequestCreationDto
import net.nifni.ours.services.FriendRequestService
import net.nifni.ours.services.UserService
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import java.util.*

@Authenticated
@Path("/users/{userId}/friendrequests/")
class FriendRequestResource(
    private val authorizer: Authorizer,
    private val friendRequestService: FriendRequestService,
    private val userService: UserService
) {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Friend request was accepted or declined successfully.",
            content = [Content(
                mediaType = "application/json",
                schema = Schema(implementation = FriendRequestResponseDto::class)
            )]
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "403",
            description = "User is not allowed to create friend requests for other users",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        ),
        APIResponse(
            responseCode = "404",
            description = "User or friend request does not exist."
        )
    )
    @Operation(summary = "Create a friend request")
    fun createFriendRequest(@PathParam("userId") userId: UUID, request: FriendRequestCreationDto): Response {
        authorizer.checkUserAuthorization(userId)?.let { return it }
        return friendRequestService.createFriendRequest(userId, request)
            .map {
                val user = userService.getUserByExternalId(userId)!!
                FriendRequestResponseDto(it.externalId, userId, FriendRequestType.SEND, user.firstname, user.lastname)
            }
            .toResponse { Response.ok().entity(it).build() }
    }

    @PUT
    @Path("/{friendRequestId}")
    @Transactional
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Friend request was accepted or declined successfully."
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "403",
            description = "User is not allowed to accept or decline the friendrequest",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        ),
        APIResponse(
            responseCode = "404",
            description = "User or friend request does not exist."
        )
    )
    @Operation(summary = "Update a friend request")
    fun updateUser(
        @PathParam("userId") userId: UUID,
        @PathParam("friendRequestId") friendRequestId: UUID,
        friendRequestUpdateDto: FriendRequestUpdateDto
    ): Response {
        val outcome = when (friendRequestUpdateDto.type) {
            FriendUpdateType.ACCEPT -> friendRequestService.acceptFriendRequest(userId, friendRequestId)
            FriendUpdateType.DECLINE -> friendRequestService.declineFriendRequest(userId, friendRequestId)
        }
        return outcome.toResponse { Response.ok().build() }
    }

    @GET
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Friend requests have been retrieved successfully.",
            content = [Content(
                mediaType = "application/json",
                schema = Schema(implementation = FriendRequestListResponseDto::class)
            )]
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "403",
            description = "User is not allowed to retrieve friend requests of other users.",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        ),
        APIResponse(
            responseCode = "404",
            description = "User does not exist."
        )
    )
    @Operation(summary = "Get friend requests which have been sent to the given user.")
    fun getFriendRequests(@PathParam("userId") userId: UUID): Response {
        return authorizer.checkUserAuthorizationWithOutcome(userId)
            .flatMap { friendRequestService.getFriendRequests(userId) }
            .map { mapFriendRequestModels(it) }
            .toResponse { Response.ok().entity(FriendRequestListResponseDto(it)).build() }
    }

    @DELETE
    @Path("/{friendRequestId}")
    @Transactional
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Friend request has been deleted successfully.",
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "403",
            description = "User is not allowed to delete friend requests of other users.",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        ),
        APIResponse(
            responseCode = "404",
            description = "Friendrequest does not exist."
        )
    )
    @Operation(summary = "Delete friend request which a user created.")
    fun deleteFriendRequest(
        @PathParam("userId") userId: UUID,
        @PathParam("friendRequestId") friendRequestId: UUID
    ): Response {
        authorizer.checkUserAuthorization(userId)?.let { return it }
        return friendRequestService.deleteFriendRequest(userId, friendRequestId)
            .toResponse { Response.ok().build() }
    }

    private fun mapFriendRequestModels(models: List<FriendRequestModel>): List<FriendRequestResponseDto> {
        return models.map {
            val user = userService.getUser(it.creator)
            FriendRequestResponseDto(
                it.externalId,
                user.externalId,
                FriendRequestType.RECEIVE,
                user.firstname,
                user.lastname
            )
        }
    }
}

enum class FriendUpdateType {
    ACCEPT,
    DECLINE
}

enum class FriendRequestType {
    SEND,
    RECEIVE
}

data class FriendRequestUpdateDto(val type: FriendUpdateType)

data class FriendRequestResponseDto(
    val id: UUID,
    @field:Schema(description = "ID of the user that created the friend request.") val creator: UUID,
    val type: FriendRequestType,
    val creatorFirstname: String,
    val creatorLastname: String
)

data class FriendRequestListResponseDto(val friendRequests: List<FriendRequestResponseDto>)
