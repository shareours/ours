package net.nifni.ours.rest.resources

import io.quarkus.security.Authenticated
import jakarta.transaction.Transactional
import jakarta.ws.rs.PUT
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import net.nifni.ours.database.user.UserModel
import net.nifni.ours.rest.Authorizer
import net.nifni.ours.rest.RequestContext
import net.nifni.ours.services.UserService
import java.time.Instant
import java.util.UUID

@Authenticated
@Path("/users")
class UserResource(
    private val authorizer: Authorizer,
    private val userService: UserService,
    private val requestContext: RequestContext
) {

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    fun updateUser(id: UUID): Response {
        authorizer.checkUserAuthorization(id)?.let { return it }
        return requestContext.getUserInformation()
            .map { userService.createOrGetUser(it) }
            .map { User.fromModel(it) }
            .toResponse { Response.ok().entity(it).build() }
    }
}

data class User(
    val id: UUID,
    val createdAt: Instant,
    val email: String,
    val firstname: String,
    val lastname: String
) {
    companion object {
        fun fromModel(userModel: UserModel): User {
            return User(
                userModel.externalId,
                userModel.createdAt,
                userModel.email,
                userModel.firstname,
                userModel.lastname
            )
        }
    }
}