package net.nifni.ours.rest.resources

import io.quarkus.security.Authenticated
import jakarta.transaction.Transactional
import jakarta.ws.rs.DELETE
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.core.Response
import net.nifni.ours.database.user.UserModel
import net.nifni.ours.rest.Authorizer
import net.nifni.ours.rest.ErrorDto
import net.nifni.ours.services.FriendshipService
import net.nifni.ours.services.UserService
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import java.util.*

@Path("/users/{userId}/friendships")
@Authenticated
class FriendshipResource(
    private val authorizer: Authorizer,
    private val friendshipService: FriendshipService,
    private val userService: UserService
) {
    @GET
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Friendships have been retrieved successfully.",
            content = [Content(
                mediaType = "application/json",
                schema = Schema(implementation = FriendshipsResponseDto::class)
            )]
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "403",
            description = "User is not allowed to retrieve friendships of other users.",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        ),
        APIResponse(
            responseCode = "404",
            description = "User does not exist."
        )
    )
    @Operation(summary = "Get friendships of a user.")
    fun getFriendships(@PathParam("userId") userId: UUID): Response {
        return authorizer.checkUserAuthorizationWithOutcome(userId)
            .flatMap { friendshipService.getFriendships(userId) }
            .map { it.map { model -> FriendshipResponseDto.fromModel(userService.getUser(model.friendId)) } }
            .toResponse { Response.ok().entity(FriendshipsResponseDto(it)).build() }
    }

    @DELETE
    @Path("/{friendId}")
    @Transactional
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Friendship has been deleted successfully."
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "403",
            description = "User is not allowed to delete friendship of other user.",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        ),
        APIResponse(
            responseCode = "404",
            description = "User does not exist."
        )
    )
    @Operation(summary = "Delete friendship between two users.")
    fun deleteFriendship(@PathParam("userId") userId: UUID, @PathParam("friendId") friendId: UUID): Response {
        return authorizer.checkUserAuthorizationWithOutcome(userId)
            .flatMap { friendshipService.deleteFriendship(userId, friendId) }
            .toResponse { Response.ok().build() }
    }
}

data class FriendshipResponseDto(val friendId: UUID, val firstname: String, val lastname: String) {
    companion object {
        fun fromModel(model: UserModel): FriendshipResponseDto {
            return FriendshipResponseDto(model.externalId, model.firstname, model.lastname)
        }
    }
}

data class FriendshipsResponseDto(val friendships: List<FriendshipResponseDto>)
