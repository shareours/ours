package net.nifni.ours.rest.resources

import io.quarkus.security.Authenticated
import jakarta.transaction.Transactional
import jakarta.ws.rs.*
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import net.nifni.ours.database.item.ItemModel
import net.nifni.ours.rest.Authorizer
import net.nifni.ours.rest.ErrorDto
import net.nifni.ours.rest.RequestContext
import net.nifni.ours.services.FriendshipService
import net.nifni.ours.services.ImageService
import net.nifni.ours.services.ItemCreationDto
import net.nifni.ours.services.ItemService
import org.eclipse.microprofile.openapi.annotations.Operation
import org.eclipse.microprofile.openapi.annotations.media.Content
import org.eclipse.microprofile.openapi.annotations.media.Schema
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses
import java.util.*

@Authenticated
@Path("/users/{userId}/items")
class ItemResource(
    private val authorizer: Authorizer,
    private val requestContext: RequestContext,
    private val itemService: ItemService,
    private val imageService: ImageService,
    private val friendshipService: FriendshipService
) {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Item was created successfully",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ItemDto::class))]
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "403",
            description = "Authenticated user is not allowed to create items for the given users",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        )
    )
    @Operation(summary = "Create Item")
    fun createItem(@PathParam("userId") userId: UUID, item: ItemCreationDto): Response {
        authorizer.checkUserAuthorization(userId)?.let { return it }
        val itemModel = itemService.createItem(userId, item)

        val externalImageId = imageService.getExternalId(itemModel.image)!!
        return Response.ok().entity(ItemDto.fromModel(itemModel, userId, externalImageId)).build()
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Items were found and retrieved",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ItemsDto::class))]
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "403",
            description = "Authenticated user is not allowed to get the items of the given users",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        ),
        APIResponse(
            responseCode = "404",
            description = "User not found"
        )
    )
    @Operation(summary = "Get Items of a user")
    fun getItems(@PathParam("userId") userId: UUID): Response {
        return authorizer.checkUserAuthorizationWithOutcome(userId)
            .onError { friendshipService.verifyBefriended(requestContext.getUserId(), userId) }
            .flatMap { itemService.getItemsByOwner(userId) }
            .map { items ->
                ItemsDto(items.map {
                    ItemDto.fromModel(it, userId, imageService.getExternalId(it.image)!!)
                })
            }.toResponse { Response.ok().entity(it).build() }
    }

    @DELETE
    @Path("/{itemId}")
    @Transactional
    @APIResponses(
        APIResponse(
            responseCode = "200",
            description = "Item was deleted successfully"
        ),
        APIResponse(
            responseCode = "401",
            description = "User is not authenticated"
        ),
        APIResponse(
            responseCode = "403",
            description = "Authenticated user is not allowed to get the items of the given users",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        ),
        APIResponse(
            responseCode = "404",
            description = "User not found",
            content = [Content(mediaType = "application/json", schema = Schema(implementation = ErrorDto::class))]
        )
    )
    @Operation(summary = "Delete item of a user")
    fun deleteItem(@PathParam("itemId") itemId: UUID, @PathParam("userId") userId: UUID): Response {
        authorizer.checkUserAuthorization(userId)?.let { return it }
        val outcome = itemService.deleteItem(userId, itemId)
        if (outcome.isError()) {
            return outcome.getError().toResponse()
        }
        return Response.ok().build()
    }
}

data class ItemDto(
    val id: UUID,
    val name: String,
    val description: String,
    val image: UUID,
    val owner: UUID
) {
    companion object {
        fun fromModel(model: ItemModel, owner: UUID, imageId: UUID): ItemDto {
            return ItemDto(model.externalId, model.name, model.description, imageId, owner)
        }
    }
}

data class ItemsDto(val items: List<ItemDto>)
