package net.nifni.ours.rest

import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.Status

data class ErrorDto(
    val status: Int,
    val code: ErrorCode,
    val message: String,
    val details: Map<String, String>
) {
    companion object {
        fun from(status: Status, errorCode: ErrorCode, message: String): Response {
            return from(status, errorCode, message, hashMapOf())
        }

        fun from(status: Status, errorCode: ErrorCode, message: String, details: Map<String, String>): Response {
            return Response.status(status)
                .entity(
                    ErrorDto(
                        status.statusCode,
                        errorCode,
                        message,
                        details
                    )
                )
                .build()
        }
    }
}

enum class ErrorCode {
    UNAUTHORIZED,
    NOT_FOUND,
    CONFLICT,
    AUTHENTICATION_MISSING,
    CLAIM_MISSING,
    SAME_USER_FRIENDREQUEST
}
