package net.nifni.ours.rest

import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.Response.Status
import net.nifni.ours.utils.Errors
import net.nifni.ours.utils.Outcome
import java.util.*

@ApplicationScoped
class Authorizer(
    private val requestContext: RequestContext
) {

    /**
     * Returns a response when the token user does not match the user in the path.
     */
    fun checkUserAuthorization(id: UUID): Response? {
        val idFromToken = requestContext.getUserId()
        if (idFromToken != id) {
            return ErrorDto.from(
                Status.FORBIDDEN,
                ErrorCode.UNAUTHORIZED,
                "Subject does not match id in path.",
                hashMapOf("subject" to idFromToken.toString(), "id" to id.toString())
            )
        }
        return null
    }

    /**
     * Returns an error outcome if the given user does not match the user in the token.
     * Otherwise, a success outcome is returned.
     */
    fun checkUserAuthorizationWithOutcome(id: UUID): Outcome<Unit> {
        val idFromToken = requestContext.getUserId()
        if (idFromToken != id) {
            return Outcome.error(Errors.NotAuthorizedError(id, idFromToken.toString()))
        }
        return Outcome.success()
    }
}