package net.nifni.ours.rest

import io.quarkus.security.UnauthorizedException
import io.quarkus.security.identity.SecurityIdentity
import jakarta.enterprise.context.ApplicationScoped
import net.nifni.ours.utils.Errors
import net.nifni.ours.utils.Outcome
import org.eclipse.microprofile.jwt.Claims
import org.eclipse.microprofile.jwt.JsonWebToken
import org.jboss.logging.Logger
import java.util.*

@ApplicationScoped
class RequestContext(
    private val securityIdentity: SecurityIdentity
) {

    private val log: Logger = Logger.getLogger(RequestContext::class.java.name)

    fun getUserId(): UUID {
        val principal = securityIdentity.principal
        if (principal is JsonWebToken) {
            return UUID.fromString(principal.subject) ?: run {
                log.error("No subject in token")
                throw UnauthorizedException()
            }
        }
        log.error("SecurityIdentity is no JsonWebToken")
        throw UnauthorizedException()
    }

    fun getUserInformation(): Outcome<UserDto> {
        val principal = securityIdentity.principal
        if (principal is JsonWebToken) {
            val userId = principal.subject ?: return createMissingClaimOutcome("subject")
            val email = principal.getClaim<String>(Claims.email) ?: return createMissingClaimOutcome("email")
            val firstname =
                principal.getClaim<String>(Claims.given_name) ?: return createMissingClaimOutcome("given_name")
            val lastname =
                principal.getClaim<String>(Claims.family_name) ?: return createMissingClaimOutcome("family_name")
            return Outcome.success(UserDto(UUID.fromString(userId), email, firstname, lastname))
        }
        log.error("SecurityIdentity is no JsonWebToken")
        return Outcome.error(Errors.AuthenticationMissingError)
    }

    private fun <T> createMissingClaimOutcome(claim: String): Outcome<T> {
        return Outcome.error(Errors.ClaimMissingError(claim))
    }
}

data class UserDto(val userId: UUID, val email: String, val firstname: String, val lastname: String)
