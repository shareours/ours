package net.nifni.ours.services

import jakarta.enterprise.context.ApplicationScoped
import net.nifni.ours.database.friendrequest.FriendRequestModel
import net.nifni.ours.database.friendrequest.FriendRequestRepository
import net.nifni.ours.rest.Authorizer
import net.nifni.ours.utils.Outcome
import java.util.*

@ApplicationScoped
class FriendRequestService(
    private val userService: UserService,
    private val friendRequestRepository: FriendRequestRepository,
    private val authorizer: Authorizer,
    private val friendshipService: FriendshipService,
    private val friendRequestValidator: FriendRequestValidator
) {
    fun createFriendRequest(externalUserId: UUID, request: FriendRequestCreationDto): Outcome<FriendRequestModel> {

        val creator = userService.getUserByExternalId(externalUserId)
            ?: return Outcome.notFound("user", externalUserId.toString())
        val friend = userService.getUserByExternalId(request.friendId)
            ?: return Outcome.notFound("user", request.friendId.toString())

        return friendRequestValidator.validateFriendRequestYourself(externalUserId, request.friendId)
            .flatMap { friendRequestValidator.validateFriendRequestNotExists(creator, friend) }
            .flatMap { friendRequestValidator.validateFriendshipNotExists(creator, friend) }
            .map { FriendRequestModel(UUID.randomUUID(), creator.id!!, friend.id!!) }
            .onSuccess { friendRequestRepository.persist(it) }
    }

    fun getFriendRequest(externalUserId: UUID, friendRequestId: UUID): FriendRequestModel? {
        val friendRequestModel = friendRequestRepository.findByExternalId(friendRequestId)
        if (friendRequestModel != null && friendRequestModel.creator != userService.getUserId(externalUserId)) {
            return null
        }
        return friendRequestModel
    }

    private fun getFriendRequestWithOutcome(
        externalUserId: UUID,
        externalFriendRequestId: UUID
    ): Outcome<FriendRequestModel> {
        val friendRequestModel = getFriendRequest(externalUserId, externalFriendRequestId)
            ?: return Outcome.notFound("friendRequest", externalFriendRequestId.toString())
        return Outcome.success(friendRequestModel)
    }

    fun acceptFriendRequest(externalUserId: UUID, externalFriendRequestId: UUID): Outcome<FriendshipDto> {
        val friendRequestModel = getFriendRequest(externalUserId, externalFriendRequestId)
            ?: return Outcome.notFound("friendRequest", externalFriendRequestId.toString())
        val externalIdOfFriend = userService.getExternalUserId(friendRequestModel.friend)
        val outcome = authorizer.checkUserAuthorizationWithOutcome(externalIdOfFriend)
            .map { friendshipService.createFriendship(friendRequestModel) }
            .onSuccess { friendRequestRepository.delete(friendRequestModel) }

        return outcome
    }

    fun getFriendRequests(externalUserId: UUID): Outcome<List<FriendRequestModel>> {
        return userService.getUserIdWithOutcome(externalUserId)
            .map { friendRequestRepository.findByFriend(it) }
    }

    fun declineFriendRequest(externalUserId: UUID, externalFriendRequestId: UUID): Outcome<Unit> {
        return getFriendRequestWithOutcome(externalUserId, externalFriendRequestId)
            .map { userService.getExternalUserId(it.friend) }
            .flatMap { authorizer.checkUserAuthorizationWithOutcome(it) }
            .onSuccess { friendRequestRepository.deleteByExternalId(externalFriendRequestId) }
    }

    fun deleteFriendRequest(externalUserId: UUID, friendRequestId: UUID): Outcome<Unit> {
        return getFriendRequestWithOutcome(externalUserId, friendRequestId)
            .map { friendRequestRepository.delete(it) }
    }
}

data class FriendRequestCreationDto(val friendId: UUID)
