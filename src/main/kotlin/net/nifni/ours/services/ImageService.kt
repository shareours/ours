package net.nifni.ours.services

import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.NotFoundException
import net.nifni.ours.database.image.ImageModel
import net.nifni.ours.database.image.ImageRepository
import java.util.*

@ApplicationScoped
class ImageService(
    private val imageRepository: ImageRepository,
    private val userService: UserService
) {
    fun createImage(image: ByteArray, externalUserId: UUID): ImageModel? {
        return userService.getUserId(externalUserId)?.let {
            val model = ImageModel(
                UUID.randomUUID(),
                image,
                it
            )
            imageRepository.persist(model)
            model
        }
    }

    fun getImage(imageId: UUID): ImageModel? {
        return imageRepository.findByExternalId(imageId)
    }

    fun getExternalId(id: Long): UUID? {
        return imageRepository.findById(id)?.externalId
    }


    fun getImageIdOrThrow(externalId: UUID): Long {
        return getImage(externalId)?.id
            ?: throw NotFoundException("Image $externalId does not exist")
    }

    fun deleteImage(id: Long) {
        imageRepository.deleteById(id)
    }
}