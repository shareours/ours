package net.nifni.ours.services

import jakarta.enterprise.context.ApplicationScoped
import net.nifni.ours.database.item.ItemModel
import net.nifni.ours.database.item.ItemRepository
import net.nifni.ours.utils.Errors
import net.nifni.ours.utils.Outcome
import java.util.*

@ApplicationScoped
class ItemService(
    private val userService: UserService,
    private val itemRepository: ItemRepository,
    private val imageService: ImageService,
) {

    fun createItem(externalUserId: UUID, item: ItemCreationDto): ItemModel {
        val userId = userService.getUserIdOrThrow(externalUserId)
        val imageId = imageService.getImageIdOrThrow(item.image)
        val itemModel = item.toModel(userId, imageId)
        itemRepository.persist(itemModel)
        return itemModel
    }

    fun getItemsByOwner(externalUserId: UUID): Outcome<List<ItemModel>> {
        return userService.getUserIdWithOutcome(externalUserId)
            .map { itemRepository.findByOwner(it) }
    }

    fun deleteItem(externalUserId: UUID, externalItemId: UUID): Outcome<Unit> {
        val userId = userService.getUserId(externalUserId)
            ?: return Outcome.error(Errors.NotFoundError("user", externalUserId.toString()))

        val itemModel: ItemModel = itemRepository.findByExternalIdAndUser(externalItemId, userId)
            ?: return Outcome.error(Errors.NotFoundError("item", externalItemId.toString()))

        itemRepository.deleteById(itemModel.id!!)
        imageService.deleteImage(itemModel.image)
        return Outcome.success()
    }
}

data class ItemCreationDto(val name: String, val description: String, val image: UUID) {
    fun toModel(owner: Long, imageId: Long): ItemModel {
        return ItemModel(
            name = name,
            image = imageId,
            description = description,
            owner = owner,
            externalId = UUID.randomUUID()
        )
    }
}