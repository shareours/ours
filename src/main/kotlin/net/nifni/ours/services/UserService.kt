package net.nifni.ours.services

import jakarta.enterprise.context.ApplicationScoped
import net.nifni.ours.database.user.UserModel
import net.nifni.ours.database.user.UserRepository
import net.nifni.ours.rest.UserDto
import net.nifni.ours.utils.Outcome
import java.util.*

@ApplicationScoped
class UserService(
    private val userRepository: UserRepository
) {
    fun createOrGetUser(userDto: UserDto): UserModel {
        return userRepository.findByExternalId(userDto.userId)?.let {
            it.email = userDto.email
            it.firstname = userDto.firstname
            it.lastname = userDto.lastname
            userRepository.persist(it)
            it
        } ?: run {
            val user = UserModel(userDto.userId, userDto.email, userDto.firstname, userDto.lastname)
            userRepository.persist(user)
            user
        }
    }

    fun getUserId(externalId: UUID): Long? {
        val user = userRepository.findByExternalId(externalId)
        return user?.id
    }

    fun getUserByExternalId(externalId: UUID): UserModel? {
        return userRepository.findByExternalId(externalId)
    }

    fun getUserIdWithOutcome(externalId: UUID): Outcome<Long> {
        return getUserId(externalId)?.let { Outcome.success(it) } ?: Outcome.notFound("user", externalId.toString())
    }

    fun getUserIdOrThrow(externalId: UUID): Long {
        return getUserId(externalId)
            ?: throw IllegalStateException("User $externalId does not exist")
    }

    fun getExternalUserId(userId: Long): UUID {
        return userRepository.findById(userId)!!.externalId
    }

    fun getUser(userId: Long): UserModel {
        return userRepository.findById(userId)!!
    }
}
