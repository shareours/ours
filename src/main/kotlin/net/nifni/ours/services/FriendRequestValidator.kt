package net.nifni.ours.services

import jakarta.enterprise.context.ApplicationScoped
import net.nifni.ours.database.friendrequest.FriendRequestRepository
import net.nifni.ours.database.user.UserModel
import net.nifni.ours.rest.ErrorCode
import net.nifni.ours.utils.Errors
import net.nifni.ours.utils.Outcome
import java.util.*

@ApplicationScoped
class FriendRequestValidator(
    private val friendRequestRepository: FriendRequestRepository,
    private val friendshipService: FriendshipService
) {

    fun validateFriendRequestYourself(creator: UUID, friend: UUID): Outcome<Unit> {
        if (creator.equals(friend)) {
            return Outcome.error(
                Errors.BadRequestError(
                    ErrorCode.SAME_USER_FRIENDREQUEST,
                    "Not allowed to add a friendrequest for yourself."
                )
            )
        }
        return Outcome.success()
    }

    fun validateFriendRequestNotExists(
        firstUser: UserModel,
        secondUser: UserModel
    ): Outcome<Unit> {
        friendRequestRepository.findByUserIds(firstUser.id!!, secondUser.id!!) ?: return Outcome.success()
        return Outcome.error(Errors.FriendRequestConflictError(firstUser.externalId, secondUser.externalId))
    }

    fun validateFriendshipNotExists(
        firstUser: UserModel,
        secondUser: UserModel
    ): Outcome<Unit> {
        if (friendshipService.areBefriended(firstUser.id!!, secondUser.id!!)) {
            return Outcome.error(Errors.FriendRequestConflictError(firstUser.externalId, secondUser.externalId))
        }
        return Outcome.success()
    }
}