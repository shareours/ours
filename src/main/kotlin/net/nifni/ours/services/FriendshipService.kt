package net.nifni.ours.services

import jakarta.enterprise.context.ApplicationScoped
import net.nifni.ours.database.friendrequest.FriendRequestModel
import net.nifni.ours.database.friendship.FriendshipModel
import net.nifni.ours.database.friendship.FriendshipRepository
import net.nifni.ours.utils.Errors
import net.nifni.ours.utils.Outcome
import java.util.*

@ApplicationScoped
class FriendshipService(
    private val friendshipRepository: FriendshipRepository,
    private val userService: UserService
) {

    fun createFriendship(friendRequestModel: FriendRequestModel): FriendshipDto {
        val first = createFriendshipModel(friendRequestModel.creator, friendRequestModel.friend)
        val second = createFriendshipModel(friendRequestModel.friend, friendRequestModel.creator)
        return FriendshipDto(first, second)
    }

    fun getFriendships(externalUserId: UUID): Outcome<List<FriendshipModel>> {
        return userService.getUserIdWithOutcome(externalUserId)
            .map { friendshipRepository.findByUser(it) }
    }

    fun areBefriended(firstUser: Long, secondUser: Long): Boolean {
        return friendshipRepository.existsByUserAndFriend(firstUser, secondUser)
    }

    fun getFriendship(externalUserId: UUID, friendId: UUID): Outcome<FriendshipModel> {
        val user = userService.getUserId(externalUserId)
            ?: return Outcome.error(Errors.NotFoundError("user", externalUserId.toString()))
        val friend = userService.getUserId(friendId)
            ?: return Outcome.error(Errors.NotFoundError("friend", friendId.toString()))
        return friendshipRepository.findByUserAndFriend(user, friend)?.let { Outcome.success(it) }
            ?: Outcome.error(
                Errors.NotFoundError(
                    "friend",
                    friendId.toString()
                )
            )
    }

    fun verifyBefriended(externalUserId: UUID, friendId: UUID): Outcome<Unit> {
        return getFriendship(externalUserId, friendId)
            .mapError { Errors.NotAuthorizedError(friendId, externalUserId.toString()) }
            .map { }
    }

    fun deleteFriendship(externalUserId: UUID, friendId: UUID): Outcome<Unit> {
        val user = userService.getUserId(externalUserId)
            ?: return Outcome.error(Errors.NotFoundError("user", externalUserId.toString()))
        val friend = userService.getUserId(friendId)
            ?: return Outcome.error(Errors.NotFoundError("friend", externalUserId.toString()))
        friendshipRepository.findByUserAndFriend(user, friend)
            ?.let { friendshipRepository.delete(it) }
        friendshipRepository.findByUserAndFriend(friend, user)
            ?.let { friendshipRepository.delete(it) }
        return Outcome.success()
    }

    private fun createFriendshipModel(user: Long, friend: Long): FriendshipModel {
        val model = FriendshipModel(UUID.randomUUID(), user, friend)
        friendshipRepository.persist(model)
        return model
    }

}

data class FriendshipDto(val first: FriendshipModel, val second: FriendshipModel)